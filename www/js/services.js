angular.module('e2.services', [])

    // Manufacturers Information
    .factory('Manufacturers', ['$http', '$settings', '$localStorage', '$filter', function ($http, $settings, $localStorage, $filter) {
        "use strict";
        return {
            all: function () {
                return $http.get($settings.webserviceEndpoint + '/get/mfgs').then(
                    function (successfulResponse) {
                        var data = successfulResponse.data;
                        if (data.session || data.session !== "undefined") {
                            $localStorage.set('session', data.session);
                        }
                        return data;
                    },
                    function (error) {
                        console.log('Error!', error);
                        throw error;
                    }
                );
            },
            get: function (mfgID) {
                return this.all().then(function (result) {
                    return $filter('filter')(result.d, function (mfg) {
                        return mfg.value === mfgID;
                    })[0];
                });
            }
        };
    }])
    // Models Information
    .factory('Models', ['$http', '$settings', '$localStorage', '$filter', function ($http, $settings, $localStorage, $filter) {
        "use strict";
        return {
            all: function (mfgID, session) {
                if (!mfgID ) { return null; }
                if (!session | session == 'undefined'){
                    session = null; // ... have to make sure the session gets passed over as a "null" object or at least a string
                }

                return $http.get($settings.webserviceEndpoint + '/get/mfgs/'+ mfgID +'/'+ session).then(
                    function (successfulResponse) {
                        var data = successfulResponse.data;
                        if (data.session || data.session !== "undefined") {
                            $localStorage.set('session', data.session);
                        }
                        return data;
                    },
                    function (error) {
                        console.log('Error!', error);
                        throw error;
                    }
                );
            },
            get: function (mfgID, modelNumber, session) {
                return this.all(mfgID, session).then(function (result) {
                    return $filter('filter')(result.d, function (model) {
                        console.log(model);
                        return model.value === modelNumber;
                    })[0];
                });
            }
        };
    }])

    .factory('Documents', ['$http', '$settings', '$localStorage', '$filter', function ($http, $settings, $localStorage, $filter) {
        "use strict";
        return {
            all: function (mfgID, modelID, session) {
                if (!mfgID | !modelID) { return null; }
                if (!session | session == 'undefined'){
                    session = null; // ... have to make sure the session gets passed over as a "null" object or at least a string
                }
                return $http.get($settings.webserviceEndpoint + '/get/mfgs/'+ mfgID + '/'+ modelID +'/'+ session).then(
                    function (successfulResponse) {
                        var data = successfulResponse.data;
                        if (data.session || data.session !== "undefined") {
                            $localStorage.set('session', data.session);
                        }
                        return data;
                    },
                    function (error) {
                        console.log('Error!', error);
                        throw error;
                    }
                );
            },
            get: function (mfgID, modelID, documentRef, session) {
                return this.all(mfgID, modelID, session).then(function (result) {
                    return $filter('filter')(result.d, function (_document) {
                        console.log(_document);
                        return model.value === documentRef;
                    })[0];
                });
            },
            getUrl: function(documentRef){
                return $http.get($settings.webserviceEndpoint + '/get/doc/'+ documentRef).then(
                    function (successfulResponse) {
                        var data = successfulResponse.data;
                        return data;
                    },
                    function (error) {
                        console.log('Error!', error);
                        throw error;
                    }
                );
            }
        };
    }])

    .factory('Parts', ['$http', '$settings', function ($http, $settings) {
        "use strict";
        return {
            get: function (partialSearch) {
                if (!partialSearch) { return null; }

                return $http.get($settings.webserviceEndpoint + '/get/part/'+ partialSearch).then(
                    function (successfulResponse) {
                        var data = successfulResponse.data;
                        return data;
                    },
                    function (error) {
                        throw  error;
                    }
                );
            }
        };
    }])
    // Constants
    .constant('SEARCH_TYPES', [
        {
            value: 'MFG',
            label: "Manufacturer"
        },
        {
            value: 'PART',
            label: "Part"
        }
    ])
    .constant('DB_CONFIG', {
        name: 'DB',
        version: '1.0',
        tables: {
            manufacturers: {
                name: 'text',
                value: 'text'
            },
            favorites: {
                mfg: 'text'
            },
            history: {
                page: 'text',
                context: 'text',
                meta: 'text',
                date_added: 'TIMESTAMP DEFAULT (datetime(\'now\',\'localtime\'))'
            }
        }
    })

    .factory('DB', function ($q, DB_CONFIG) {
        var self = this;
        self.db = null;
        self.init = function () {
            if (window.sqlitePlugin) {
                self.db = window.sqlitePlugin.openDatabase({name: DB_CONFIG.name});
            } else if (window.openDatabase) {
                self.db = window.openDatabase(DB_CONFIG.name, DB_CONFIG.version, 'database', -1);
            }

            for (var tableName in DB_CONFIG.tables) {
                var defs = [];
                var columns = DB_CONFIG.tables[tableName];
                for (var columnName in columns) {
                    var type = columns[columnName];
                    defs.push(columnName + ' ' + type);
                }
                var sql = 'CREATE TABLE IF NOT EXISTS ' + tableName + ' (' + defs.join(', ') + ')';
                self.query(sql);
            }
        };

        self.insertAll = function (tableName, data) {
            var columns = [],
                bindings = [];

            for (var columnName in DB_CONFIG.tables[tableName]) {
                columns.push(columnName);
                bindings.push('?');
            }

            var sql = 'INSERT INTO ' + tableName + ' (' + columns.join(', ') + ') VALUES (' + bindings.join(', ') + ')';

            self.db.transaction(function (transaction) {
                for (var i = 0; i < data.length; i++) {
                    var values = [];
                    for (var j = 0; j < columns.length; j++) {
                        values.push(data[i][columns[j]]);
                    }

                    transaction.executeSql(sql, values);
                }
            }, function(error){
                if (error){
                    console.log(error);
                }
            }, function(results){
                if (results){
                    console.log(results);
                }
            });
        };

        self.query = function (sql, bindings) {
            bindings = typeof bindings !== 'undefined' ? bindings : [];
            var deferred = $q.defer();

            self.db.transaction(function (transaction) {

                transaction.executeSql(sql, bindings, function (transaction, result) {
                    deferred.resolve(result);
                }, function (transaction, error) {
                    console.log(error);
                    deferred.reject(error);
                });
            });

            return deferred.promise;
        };

        self.fetchAll = function (result) {
            var output = [];

            for (var i = 0; i < result.rows.length; i++) {
                output.push(result.rows.item(i));
            }

            return output;
        };

        return self;
    })

    // Local Storage (DB)
    .factory('$localStorage', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            remove: function (key) {
                $window.localStorage.removeItem(key);
            }
        }
    }])
    .factory('$settings', ['$localStorage', function ($localStorage) {

        var internalFunctions = {
            getHistory: function () {
                var HISTORY = 'history';
                if (!$localStorage.getObject(HISTORY)) {
                    $localStorage.setObject(HISTORY, {})
                }
                return $localStorage.getObject(HISTORY);
            }
        };
        var DEFAULT_ENDPOINT = 'defaultEndpoint';
        return {
            webserviceEndpoint: 'http://104.236.195.56/.ws',
            defaultEndpoint: {
                get: function(){
                    if (!$localStorage.get(DEFAULT_ENDPOINT, null)) {
                        $localStorage.set(DEFAULT_ENDPOINT, 'http://e2.davisware.com');
                    }
                    return $localStorage.get(DEFAULT_ENDPOINT, null)
                },
                set: function (newValue) {
                    return $localStorage.set(DEFAULT_ENDPOINT, newValue);
                }
            },
            history: internalFunctions.getHistory(),
            session: $localStorage.get('session'),
            version: 'v0.1a',
            isDev: $localStorage.get('isDev', false),
            defaultSearchType: {
                get: function(){
                    return $localStorage.get('defaultSearchType', 'MFG');
                },
                set: function (newValue) {
                    return $localStorage.set('defaultSearchType', newValue);
                }
            }
        };
    }]);
