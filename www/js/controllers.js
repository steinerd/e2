angular.module('e2.controllers', [])

    // History Controller
    .controller('HistoryCtrl', function ($scope, DB, $ionicLoading, $settings) {

        $ionicLoading.show({
            template: 'Loading History..',
            scope: $scope
        });
        $scope.history;

        $scope.doRefresh = function(){
            DB.query('SELECT * FROM history').then(function (results) {
                var historyFromDB = DB.fetchAll(results);

                angular.forEach(historyFromDB, function(value, key){
                    historyFromDB[key].metadata = angular.fromJson(value.meta);
                });
                $scope.history = historyFromDB;

                $scope.$broadcast('scroll.refreshComplete');
                $ionicLoading.hide();
            });
        };

        $scope.openPartDetail = function(partNo){
            window.open($settings.defaultEndpoint.get() + '/Search/PartDetails.aspx?pno='+partNo, '_blank','location=no,closebuttoncaption=Close,enableViewportScale=yes');
        };

        $scope.$on('$ionicView.enter', function() {
            $scope.history = null;
            $scope.doRefresh();
        });
    })

    // Search Controller
    .controller('SearchCtrl', function ($scope,
                                        $rootScope,
                                        $ionicPlatform,
                                        $ionicNavBarDelegate,
                                        $ionicSideMenuDelegate,
                                        $ionicLoading,
                                        $ionicFilterBar,
                                        $ionicFilterBarConfig,
                                        $timeout,
                                        $filter,
                                        $settings,
                                        $localStorage,
                                        $ionicPopup,
                                        $window,
                                        DB,
                                        Manufacturers,
                                        Parts,
                                        Documents) {
        // Instantiate locals
        var filterBarInstance;
        var lastEndpoint;

        //#region Functions
        // ----------
        $scope.hideLoading = function(){
            $scope.$broadcast('scroll.refreshComplete');
            $ionicLoading.hide();
        };

        // This action will allow the search page to refresh on command
        //  and make sure the database is the freshest it can be.
        $scope.doRefresh = function () {
            DB.query('DELETE FROM manufacturers');

            $scope.mfgs = null;
            $scope.models = null;
            $scope.parts = {};

            Manufacturers.all().then(function (results) {
                DB.insertAll('manufacturers', results.d);
                DB.query('SELECT * FROM manufacturers').then(function (results) {
                    $scope.mfgs = DB.fetchAll(results);
                    $scope.hideLoading();
                });
            }, function(error){
                $scope.hideLoading();
                $ionicPopup.alert({
                    title: 'Connection Error',
                    template: 'Cannot establish connection to server, Check internet connection(s).' +
                    '<br/><br />This application requires an internet connection. Contact support if problem persists.'
                });
            });
        };


        $scope.openPartDetail = function(part){
            var fullURL = $settings.defaultEndpoint.get() + '/Search/PartDetails.aspx?pno='+part.partno;
            var ref = window.open(fullURL, '_blank','location=no,closebuttoncaption=Close,enableViewportScale=yes');
            ref.addEventListener('loadstop', function () {
                DB.query('DELETE FROM history WHERE page=? AND context=?', [fullURL, 'PART']).then(function (results) {
                    console.log(results);
                    var docMetaData = {
                        name: part.name,
                        partno: part.partno
                    };
                    DB.insertAll('history', [{page: fullURL, context: 'PART', date_added: Date.now(), meta: JSON.stringify(docMetaData)}]);
                });

            });
        };
        $scope.showFilterBar = function () {
            switch ($scope.currentSearchType.value){
                case 'MFG':
                    filterBarInstance = $ionicFilterBar.show({
                        items: $scope.mfgs,
                        update: function (filteredItems) {
                            $scope.mfgs = filteredItems;
                        }
                    });
                    break;
                case 'PART':
                    filterBarInstance = $ionicFilterBar.show({
                        delay: 2500,
                        items: $scope.parts,
                        update: function (filteredItems, filteredText) {
                            if(!filteredText){ return; }
                            $ionicLoading.show({
                                template: 'Searching...<br /><small>Query: {filteredText}</small>'.replace('{filteredText}', filteredText),
                                scope: $scope
                            });
                            Parts.get(filteredText).then(function(results){
                                $scope.parts = results.d;
                                $scope.$broadcast('scroll.refreshComplete');
                                $ionicLoading.hide();
                            },function(err){
                                $ionicPopup.alert({
                                    title: 'Error',
                                    template: 'Cannot find any parts with the information provided...'
                                });
                                console.log(err);
                                $scope.$broadcast('scroll.refreshComplete');
                                $ionicLoading.hide();
                            });
                        },
                        cancel: function(){
                            $scope.parts = {};
                        }
                    });
                    break;
            }
        };

        $scope.refreshItems = function () {
            if (filterBarInstance) {
                filterBarInstance();
                filterBarInstance = null;
            }

            $timeout(function () {
                $scope.doRefresh();
                $scope.$broadcast('scroll.refreshComplete');
            }, 1000);
        };
        //#endregion


        $ionicLoading.show({
            template: 'Loading...<br /><small>{defaultEndpoint}</small>'.replace('{defaultEndpoint}', $settings.defaultEndpoint.get()),
            scope: $scope
        });

        $scope.$watch(function(){ return $rootScope.model.searchType; }, function(newValue, oldValue){
            if (newValue !== oldValue){
                $scope.currentSearchType = $rootScope.model._get();
            }
        });
        //
        //$scope.$on('updatedEndpoint', function(event, args) {
        //    $scope.doRefresh();
        //});

        $scope.$on('$ionicView.enter', function() {
            // Code you want executed every time view is opened
            if(!lastEndpoint){
                lastEndpoint = $settings.defaultEndpoint.get();
            }
            var currentEndpoint = $settings.defaultEndpoint.get();
            if(lastEndpoint !== currentEndpoint){
                lastEndpoint = null;
                $ionicLoading.show({
                    template: 'Loading...<br /><small>{defaultEndpoint}</small>'.replace('{defaultEndpoint}', currentEndpoint),
                    scope: $scope
                });
                $scope.doRefresh();
            }
        });

        $ionicPlatform.ready(function () {
            $scope.currentSearchType = $rootScope.model._get();
            $scope.doRefresh();
        });

    })

    // Search - Child Details
    .controller('SearchDetailCtrl', function ($scope, $stateParams, $ionicLoading, $ionicPopup, $ionicHistory , $ionicPlatform, $timeout, $settings, $location, Models) {
        $scope.pageTitle = $stateParams.mfgName;
        $scope.mfgID = $stateParams.mfgID;
        $scope.mfgName = $stateParams.mfgName;
        $scope.session = $settings.session;

        var showEmptyAlert = function(){
            $ionicPopup.alert({
                title: 'Warning',
                template: 'Cannot find any models for this manufacturer...'
            }).then(function (result) {
                $location.path('/tab/search');
            });
        };

        $scope.doRefreshModels = function(){
            Models.all($scope.mfgID, $scope.session).then(function(result){
                if(!result || result == 'undefined' || result.d.length == 0){
                    showEmptyAlert();
                }

                if(result.d){
                    $scope.models = result.d;
                }else{
                    if(result.d.length == 0) {
                        showEmptyAlert();
                    }
                }

                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            }, function(err){
                console.log(err);
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');

                // Run Error Alert
                $ionicPopup.alert({
                    title: 'Error!',
                    template: err.message
                }).then(function(result){
                    $ionicHistory.backView();
                });
            });
        };

        $scope.backButtonClick = function () {
            $location.path('/tab/search');
        };

        $scope.backButtondblClick = function(){
            $location.path('/tab/search');
        };

        $ionicLoading.show({
            template: 'Loading...<br/><small>Fetching Models</small>',
            scope: $scope
        });

        $ionicPlatform.ready(function () {
            $scope.doRefreshModels();
        });

    })

    // Search - Child Details
    .controller('SearchDetailDocumentCtrl', function ($scope, $stateParams, $location, $ionicLoading, $ionicPlatform, $timeout, $settings, Documents) {
        $scope.pageTitle = $stateParams.mfgName;
        $scope.mfgID = $stateParams.mfgID;
        $scope.mfgName = $stateParams.mfgName;
        $scope.session = $settings.session;
        $scope.modelNumber = $stateParams.modelNumber;
        $scope.documents = null;

        var showEmptyAlert = function(){
            $ionicPopup.alert({
                title: 'Warning',
                template: 'Cannot find any documents for this model...'
            }).then(function (result) {
                $location.path('/tab/search/'+ $scope.mfgID +'/'+ $scope.mfgName);
            });
        };

        $ionicLoading.show({
            template: 'Loading...<br /><small>Fetching documents</small>',
            scope: $scope
        });

        $scope.doRefreshDocs = function(){
            var getAllDocs = Documents.all($scope.mfgID, $scope.modelNumber, $scope.session);
            if (getAllDocs){
                getAllDocs.then(function(result){
                    if(!result || result == 'undefined' || result.d.length == 0){
                        showEmptyAlert();
                    }

                    if(result.d){
                        $scope.documents = result.d;
                    }else{
                        if(result.d.length == 0) {
                            showEmptyAlert();
                        }
                    }

                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                }, function(err){
                    console.log(err);
                    $ionicLoading.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
            }
        };

        $scope.backButtonClick = function () {
            $location.path('/tab/search/'+ $scope.mfgID +'/'+ $scope.mfgName);
        };

        $scope.backButtondblClick = function(){
            $location.path('/tab/search');
        };


        //Manufacturers.get($stateParams.mfgID).then(function (result) {
        //    $ionicLoading.hide();
        //    $scope.mfg = result;
        //});

        $ionicPlatform.ready(function () {
            $scope.doRefreshDocs();
        });

    })

    .controller('DocumentCtrl', function ($scope, $stateParams, $settings, $ionicLoading, $ionicPlatform, $timeout, $filter, $window, $ionicNavBarDelegate, $ionicHistory, Documents, DB) {
        $scope.pageTitle = $stateParams.mfgName;
        $scope.mfgID = $stateParams.mfgID;
        $scope.mfgName = $stateParams.mfgName;
        $scope.modelNumber = $stateParams.modelNumber;
        $scope.docRef = $stateParams.docRef;
        $scope.docName = $filter('base64Decode')( $stateParams.docName);

        $ionicLoading.show({
            template: 'Loading...<br/><small>Streaming Document</small>',
            scope: $scope
        });

        $scope.frameheight = $window.innerHeight;
        $ionicPlatform.ready(function () {
            Documents.getUrl($scope.docRef).then(function(result){
                if (result && result.document_url)
                {
                    var deviceInformation = ionic.Platform.device();
                    var _openContext = '_blank';


                    var _url = result.document_url;
                    var _fullUrl = 'https://docs.google.com/viewer?embedded=true&url='+encodeURIComponent(_url);
                    var ref;

                    if ($settings.isDev){
                        $ionicLoading.hide();
                    }else{
                        ref = window.open(_fullUrl, _openContext,'location=no,closebuttoncaption=Close,enableViewportScale=yes,hidden=yes');
                    }

                    var docMetaData = {
                        mfg_name: $scope.mfgName,
                        mfg_id: $scope.mfgID,
                        model_number: $scope.modelNumber,
                        document_ref: $scope.docRef,
                        document_name: $scope.docName
                    };


                    DB.query('DELETE FROM history WHERE page=? AND context=?', [_fullUrl, 'DOC']).then(function (results) {
                        console.log(results);
                        DB.insertAll('history', [{page: _fullUrl, context: 'DOC', date_added: Date.now(), meta: JSON.stringify(docMetaData)}]);
                    });

                    if (!$settings.isDev) {
                        ref.addEventListener('exit', function (event, doc) {
                            $ionicHistory.goBack();
                        });

                        ref.addEventListener('loadstop', function () {
                            $ionicLoading.hide();
                            ref.show();
                            ref.insertCSS({code: '[role="toolbar"] { display: none !important; }'});
                        });
                    }


                }
            });
        });
    })


    // Settings Controller
    .controller('SettingsCtrl', function ($scope, $settings, $http, DB, $ionicPopup) {

        var _defaultEndpoint = $settings.defaultEndpoint.get();
        $scope.model ={
            defaultEndpoint: _defaultEndpoint
        };

        $scope.settings = {
            enableFriends: true,
            version: $settings.version
        };

        $scope.clearHistory = function(){
            DB.query('DELETE FROM history').then(function (results) {
                $ionicPopup.alert({
                    title: 'Operation Successful',
                    template: 'History has been successfully cleared!'
                });
            });
        };

        $scope.$on('$ionicView.beforeLeave', function() {
            // Code you want executed every time before we jump away from this view
            var currentEndpoint = $settings.defaultEndpoint.get();
            var newEndpoint = $scope.model.defaultEndpoint;
            if(newEndpoint !== currentEndpoint){
                $settings.defaultEndpoint.set(newEndpoint);
                $http.defaults.headers.common.Client = newEndpoint;
            }
        });
    });