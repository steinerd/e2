// Ionic E2 App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'e2' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'e2.services' is found in services.js
// 'e2.controllers' is found in controllers.js
angular.module('e2', ['ionic', 'e2.controllers', 'e2.services', 'jett.ionic.filter.bar'])
    .run(function ($ionicPlatform, $rootScope, $timeout, $http, $ionicNavBarDelegate, $ionicSideMenuDelegate, $filter, $localStorage, $settings, DB, SEARCH_TYPES) {
        "use strict";

        $http.defaults.headers.common.Client = $settings.defaultEndpoint.get();
        DB.init();

        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }

            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleLightContent();
            }

        });

        var defaultSearchType = $settings.defaultSearchType.get();
        $rootScope.searchTypeSideBarInstance = null;

        $rootScope.searchTypes = SEARCH_TYPES;
        $rootScope.model = {
            searchType: defaultSearchType,
            searchTypeChange: function(){
                var leftMenu  = $ionicSideMenuDelegate.$getByHandle('searchTypeSideMenu');
                $settings.defaultSearchType.set(this.searchType);
                var _title = this._get();
                $ionicNavBarDelegate.title(_title.label + " Search");
                if (leftMenu._instances.length === 1){
                    var menuInstance = leftMenu._instances[0];
                    $rootScope.searchTypeSideBarInstance = menuInstance;
                    $timeout(function(){
                        menuInstance.toggleLeft();
                    }, 100);

                }
            },
            _get: function(){
                var _title = $filter('filter')($rootScope.searchTypes, function (_searchType) {
                    return _searchType.value === $rootScope.model.searchType;
                })[0];
                return _title;
            }
        };


    })
    .filter('base64Encode', function() {
        return window.btoa;
    })
    .filter('base64Decode', function() {
        return window.atob;
    })
    .config(function ($stateProvider, $urlRouterProvider) {
        "use strict";

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider
            // setup an abstract state for the tabs directive
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/tabs.html'
            })

            // Each tab has its own nav history stack:

            .state('tab.history', {
                url: '/history',
                views: {
                    'tab-history': {
                        templateUrl: 'templates/tab-history.html',
                        controller: 'HistoryCtrl'
                    }
                }
            })

            .state('tab.search', {
                url: '/search',
                views: {
                    'tab-search': {
                        templateUrl: 'templates/tab-search.html',
                        controller: 'SearchCtrl'
                    }
                }
            })
                .state('tab.search-detail', {
                    url: '/search/:mfgID/:mfgName',
                    views: {
                        'tab-search': {
                            templateUrl: 'templates/search-detail.html',
                            controller: 'SearchDetailCtrl'
                        }
                    }
                })
                    .state('tab.search-detail-documents', {
                        url: '/search/:mfgID/:mfgName/:modelNumber',
                        views: {
                            'tab-search': {
                                templateUrl: 'templates/search-detail-documents.html',
                                controller: 'SearchDetailDocumentCtrl'
                            }
                        }
                    })
                        .state('tab.search-detail-document', {
                            url: '/search/:mfgID/:mfgName/:modelNumber/:docRef/:docName',
                                views: {
                                    'tab-search': {
                                        templateUrl: 'templates/search-document.html',
                                        controller: 'DocumentCtrl'
                                    }
                                }
                            })

            .state('tab.settings', {
                url: '/settings',
                views: {
                    'tab-settings': {
                        templateUrl: 'templates/tab-settings.html',
                        controller: 'SettingsCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/search');
    });